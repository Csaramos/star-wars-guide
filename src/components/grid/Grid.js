import './Grid.scss'


const Grid = ({ gap = 15, columns = 1, rows = 1, children }) => {
    const gridStyle = {
        display: 'grid',
        gridGap: `${gap}px`,
        gridTemplateColumns: `repeat(${columns}, 1fr)`,
        gridTemplateRows: `repeat(${rows}, 1fr)`,

    }
    return (
        <div className='grid-container' style={gridStyle}>
            {children}
        </div>
    )
}

export default Grid