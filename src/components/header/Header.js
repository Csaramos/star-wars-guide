import './Header.scss'
import background from '../../Assets/Images/space-background.jpeg'
import { Link } from 'react-router-dom'

const Header = () => {
    return (
        <header className='header-container' style={{ backgroundImage: `url(${background})` }}>
            <Link to='/'><h1 className='main-title'>Star Wars Guide</h1></Link>
        </header>
    )
}

export default Header