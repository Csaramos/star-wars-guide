import Container from '../../components/container/Container'
import Grid from '../../components/grid/Grid'
import Header from '../../components/header/Header'
import './Home.scss'

const Home = () => {
    return (
        <div>
            <Header />
            <Container>
                <Grid gap={25} columns={3} rows={2}>
                    <div>1</div>
                    <div>2</div>
                    <div>3</div>
                    <div>4</div>
                    <div>5</div>
                    <div>6</div>
                </Grid>
            </Container>
        </div>
    )
}

export default Home